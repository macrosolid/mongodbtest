from django.apps import AppConfig


class HwpageConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'hwpage'
