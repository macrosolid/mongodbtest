db.createUser({
    user: 'admin',
    pwd: '$DEFAULT_MON_PASS',
    roles: [
        {
            role: 'readWrite',
            db: 'helloword',
        },
    ],
});

db = new Mongo().getDB("helloword");

db.createCollection('phrases', { capped: false });

db.phrases.insert([
    { 
        "item": "Hello World",
        "type": "Phrase"
    }
]);

db.createUser({
    user: "test",
    pwd: "testing",
    roles: [
        {
            role: "clusterMonitor",
            db: "admin" 
        },
        { 
            role: "read", 
            db: "local" 
        }
    ]
});
