#!/bin/bash
username=ubuntu
dockername=docker
if [[ "$OSTYPE" == "linux-gnu"* ]]; then
        if cut -d: -f1 /etc/group | grep "^$dockername$";
        then
                sudo echo "Group '$dockername' is already exists, it seems '$dockername' already set. Bye."
                sudo chown -R $username: /opt/monitoring
                # rm /opt/hwdjango/docker-compose.yml
        else
                sudo mkdir /opt/monitoring
                sudo chown -R $username: /opt/monitoring
                # Ubuntu setting, install applicatins, docker, docker compose + etc.
                sudo apt-get update
                sudo apt install curl software-properties-common ca-certificates apt-transport-https -y
                curl -f -s -S -L https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
                sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu jammy stable"
                sudo apt update
                apt-cache policy docker-ce
                sudo apt install docker-ce -y
                sudo systemctl status docker
                sudo curl -L «https://github.com/docker/compose/releases/download/v2.12.2/docker-compose$(uname -s)-$(uname -m)» -o /usr/local/bin/docker-compose
                mkdir -p ~/.docker/cli-plugins/
                curl -SL https://github.com/docker/compose/releases/download/v2.3.3/docker-compose-linux-x86_64 -o ~/.docker/cli-plugins/docker-compose
                chmod +x ~/.docker/cli-plugins/docker-compose
                docker compose version
                sudo usermod -G $dockername $username
                # sudo -o StrictHostKeyChecking=no /tmp/sit_backend.tar.gz ubuntu@52.90.149.56
                echo "'$username' is set"
        fi
else
        echo "This script only for linux-gnu not for $OSTYPE"
fi
